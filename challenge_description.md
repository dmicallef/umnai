# Coding Challenge

The ACME Vehicle Fleet Inc. company specialises in leasing land vehicles, including cars, trucks, and motorcycles. Recent success has prompted the company to expand its fleet and the creation of a system to track it. The current company offerings include cars, trucks, and motorcycles. The individual
vehicle details are listed below:

- Car
    + VIN (alpha-numeric)
    + Make
    + Model
    + Year
    + Seat Capacity
    + Roof Rack Availability (yes/no)

- Truck
    - VIN (alpha-numeric)
    - Make
    - Model
    - Year
    - Seat Capacity
    - Haul Capacity (kg)

- Motorcycle
    + VIN (alpha-numeric)
    + Make
    + Model
    + Year
    + Seat Capacity
    + Sidecar availability (yes/no)

Note that VIN is unique across all vehicles.

## Excercise

Create a REST API application using Python to provide the following operations:

1. View vehicles. This operation should also support:
    a. Pagination
    b. Filtering (by)
        i. Type
        ii. Make
    c. Sorting (by)
        i. Year
2. View an existing vehicle (via VIN).
3. Create a new vehicle.
4. Delete an existing vehicle.
5. Update an existing vehicle.
6. Replace an existing vehicle.

The application should make use of a **MongoDB** database to persist the required information.
