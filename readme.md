## Challenge
For a description about the challenge please take a look at `challenge_description.md` file

## Introduction

Notes: 
- Project tested using Python 3.8+
- Tested on MacOS BigSur
- NB: If running without docker a library which reads key-value pairs from a .env file is required. I am using `python-dotenv==0.15.0` 
  (ref: https://pypi.org/project/python-dotenv/) and the instructions below will assume as such.
- Ports 8000 and 27017 are required for the application to run as described below.

## Quickstart

## Running the application using Docker
 0. Make sure ports 8000 and 27017 are not occupied
 1. Copy contents from example file `.env-development-example` to your `umnai/.env` file
 2. Make sure you have `python-dotenv==0.15.0` (or similar) installed   
 3. Run application using `docker-compose up --build`

Note that the `django` container simply runs a development environment. `uwsgi` and `nginx` are not set up for this
project.

## Manual Setup
- From project root (e.g. `~/umnai/`) setup a Virtual Environment and activate it.
- Change directory to `~/umnai/acme`
- Install requirements `pip3 install -r requirements.txt`
- Make sure `python-dotenv` or equivalent is installed
  
#### Database setup
- Go to project root and `docker-compose up`. This will start the mongo database.
- Run Migrations `dotenv run python3 manage.py migrate`

## API
Once the server is running go to `/api/`. The API interface (provided by DRF) should be self-explanatory. Nonetheless I
have added a few examples below.

Example Usage:

- Listing all Vehicles `GET /api/vehicles/`

Using the `vin` number the user can `get`, `update` or `delete` a specific instance.
- View an existing vehicle: `GET /api/vehicles/<vin>/`
- Create a new vehicle `POST /api/vehicles/`
    - Data example: 
  ```
  {
        "vin": "1HGBH41XMN109189",
        "type": "truck",
        "make": "Isuzu",
        "model": "Elf",
        "year": 2012,
        "seat_capacity": 3,
        "roof_rack_availibility": false,
        "haul_capacity": 5000,
        "sidecar_availibility": false
    }
  ```
- Delete an existing vehicle  VIN `DELETE /api/vehicles/<vin>/`
- Update an existing vehicle `PATCH /api/vehicles/<vin>/`
- Replace an existing vehicle `PUT /api/vehicles/<vin>/`

### Pagination
Pagination is set to 100 by default. The `next` item in the response json specifies the next url.
Example: `/api/vehicles/?page=2` will retrieve the second batch of vehicles


### Filtering
Use parameters `make` and `type` to filter be able to filter

Example: `GET /api/vehicles/?make=my-car-make&type=car`


### Ordering
  Order Descending
    `GET /api/vehicles/?ordering=-year`
  
  Order Ascending
    `GET /api/vehicles/?ordering=year`

# Running Test
Before attempting to run tests make sure all containers from previous run are stopped (check using `docker ps`). If this
is not the case use `docker-compose stop`.

 1. Copy contents from example file `.env-test-example` to your `umnai/.env` file
 2. Make sure you have `python-dotenv==0.15.0` (or similar) installed     
 3. Run tests using `docker-compose.test.yml`
    `dotenv run docker-compose run test bash -c "pytest -v /app/acme/"`
 4. Stop containers (inorder to avoid mongo port conflict):`dotenv run docker-compose stop`
    
# Future Improvements and Design decisions

1. Mongo `username/password` are hardcoded to the `js` file atm. This needs to be updated via env variables for production use.
2. Validation needs to be added to make sure that `Sidecar Availability` is always `null` unless type is motorcycle. The 
   same goes for `Roof rack Availability` with `car` and `Haul Capacity` with `Truck`.