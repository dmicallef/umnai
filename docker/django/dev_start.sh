#!/bin/bash
set -eux
if ! /bin/ls src/acme.egg-info; then
    echo "Installing Acme in src/acme.egg-info"
    python3 setup.py develop
else
    echo "ACME is already installed in src/acme.egg-info"
fi
django-admin migrate --noinput
cd /app/acme
exec django-admin runserver 0.0.0.0:8000 "$@"
