// Todo: Use sh script to retrieve username and password via environment variables
db.createUser(
    {
        user: "mongoadmin",
        pwd: "secret",
        roles: [
            {
                role: "readWrite",
                db: "acmedb"
            }
        ]
    }
)