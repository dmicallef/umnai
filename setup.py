#!/usr/bin/env python
# -*- encoding: utf-8 -*-
import os
from setuptools import setup

with open(os.path.join(os.path.dirname(__file__), 'VERSION')) as version_file:
    version = version_file.read()

with open(os.path.join(os.path.dirname(__file__), 'requirements.txt')) as requirements_file:
    requirements = requirements_file.readlines()


if __name__ == "__main__":
    setup(
        name='ACME',
        version=version,
        description='ACME Vehicle registration',
        author='Daniel Micallef',
        package_dir={'': 'acme'},
        python_requires='~=3.8',
    )
