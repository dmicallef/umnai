import pytest
from rest_framework.test import APIClient, APIRequestFactory


@pytest.fixture
def api_client():
    return APIClient()


@pytest.fixture
def api_factory():
    return APIRequestFactory()


pytest_plugins = [
    'acmevehicle.tests.factories',
]
