from acmevehicle.models import Vehicle
from acmevehicle.serializers import VehicleSerializer
from rest_framework import viewsets


class VehicleViewSet(viewsets.ModelViewSet):
    serializer_class = VehicleSerializer
    filterset_fields = ['type', 'make']

    def get_queryset(self):
        queryset = Vehicle.objects.all()
        return queryset


# Todo / To test:
# View Vehicles
# View an existing vehicle via VIM
# Create a new Vehicle
# Delete an existing Vehicle
# Update an existing Vehicle
# Replace an existing Vehicle
