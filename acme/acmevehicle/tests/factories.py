import factory
import pytest_factoryboy
from acmevehicle.models import Vehicle
from datetime import datetime


@pytest_factoryboy.register
class VehicleFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Vehicle

    vin = factory.Sequence(lambda n: f'1HGBH41XMN1091{n}')
    type = factory.Iterator(['car', 'truck', 'motorcycle'])
    make = factory.Iterator(['Fiat', 'Honda', 'Tesla', 'Isuzu'])
    model = factory.Sequence(lambda n: f'model {n}')
    year = 2010
    seat_capacity = 5

    roof_rack_availibility = None
    haul_capacity = None
    sidecar_availibility = None
    ctime = datetime.now()
    mtime = datetime.now()


@pytest_factoryboy.register
class CarVehicleFactory(VehicleFactory):
    type = 'car'
    roof_rack_availibility = True


@pytest_factoryboy.register
class TruckVehicleFactory(VehicleFactory):
    type = 'truck'
    haul_capacity = 5000


@pytest_factoryboy.register
class MotorCycleVehicleFactory(VehicleFactory):
    type = 'motorcycle'
    sidecar_availibility = True
