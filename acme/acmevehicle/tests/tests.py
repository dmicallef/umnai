import pytest
from acmevehicle.models import Vehicle
from django.urls import reverse


def get_url_by_vin(vin):
    return f"{reverse('vehicles-list')}{vin}/"


@pytest.mark.django_db
def test_vehicle_get_ok(api_client, vehicle_factory):
    number_of_vehicles = 6
    vehicle_factory.create_batch(size=number_of_vehicles)
    url = reverse('vehicles-list')
    response = api_client.get(url, format='json')
    assert response.status_code == 200
    assert response.data.get('count') == number_of_vehicles


@pytest.mark.parametrize('type', ['car', 'truck', 'motorcycle'])
@pytest.mark.django_db
def test_vehicle_filter_type(api_client, vehicle_factory, type):
    number_of_vehicles = 6
    vehicle_factory.create_batch(size=number_of_vehicles)
    url = f'{reverse("vehicles-list")}?type={type}'
    response = api_client.get(url, format='json')
    assert response.status_code == 200
    assert response.data.get('count') == 2
    assert {result.get('type') for result in response.data.get('results')} == {type}


@pytest.mark.django_db
def test_vehicle_filter_make(api_client, vehicle_factory):
    make = 'my-car-make'
    vehicle_factory.create_batch(size=4)
    vehicle_factory.create_batch(size=2, make=make)
    url = f'{reverse("vehicles-list")}?make={make}'
    response = api_client.get(url, format='json')
    assert response.status_code == 200
    assert response.data.get('count') == 2
    assert {result.get('make') for result in response.data.get('results')} == {make}


@pytest.mark.django_db
def test_get_existing_vehicle(api_client, vehicle_factory):
    vehicles = vehicle_factory.create_batch(size=3)
    test_vehicle = vehicles[0]
    vin = test_vehicle.vin
    resp = api_client.get(get_url_by_vin(vin))
    assert resp.status_code == 200
    data = resp.data
    for item in data.keys():
        assert data[item] == getattr(test_vehicle, item)


@pytest.mark.parametrize('ordering', ['asc', 'desc'])
@pytest.mark.django_db
def test_ordering_by_year(api_client, vehicle_factory, ordering):
    # /api/vehicles/?ordering=-year`
    base_url = reverse("vehicles-list")
    order = 'year' if ordering == 'asc' else '-year'
    url = f'{base_url}?ordering={order}'

    years = [2010, 2011, 2012]
    for year in years:
        vehicle_factory.create(year=year)

    response = api_client.get(url)
    expected_year = years[0] if ordering == 'asc' else years[2]
    assert response.data.get('results')[0].get('year') == expected_year


@pytest.mark.django_db
def test_create_new_vehicle(api_client):
    url = reverse('vehicles-list')
    data = {
        "vin": "1HGBH41XMN109189",
        "type": "truck",
        "make": "Isuzu",
        "model": "Elf",
        "year": 2012,
        "seat_capacity": 3,
        "roof_rack_availibility": False,
        "haul_capacity": 5000,
        "sidecar_availibility": False,
    }
    resp = api_client.post(url, data, format='json')
    assert resp.status_code == 201
    assert Vehicle.objects.count() == 1


@pytest.mark.django_db
def test_replace_new_vehicle(api_client, vehicle):
    updated_year = vehicle.year + 1
    data = {
        "vin": vehicle.vin,
        "type": vehicle.type,
        "make": vehicle.make,
        "model": vehicle.model,
        "year": updated_year,
        "seat_capacity": vehicle.seat_capacity,
        "roof_rack_availibility": vehicle.roof_rack_availibility,
        "haul_capacity": vehicle.haul_capacity,
        "sidecar_availibility": vehicle.sidecar_availibility,
    }
    url = get_url_by_vin(vehicle.vin)
    resp = api_client.put(url, data, format='json')
    assert resp.status_code == 200
    vehicle.refresh_from_db()
    assert vehicle.year == updated_year


@pytest.mark.django_db
def test_patch_new_vehicle(api_client, vehicle):
    updated_year = vehicle.year + 1
    data = {
        "year": updated_year,
    }
    url = get_url_by_vin(vehicle.vin)
    resp = api_client.patch(url, data, format='json')
    assert resp.status_code == 200
    vehicle.refresh_from_db()
    assert vehicle.year == updated_year
