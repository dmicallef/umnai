from django.apps import AppConfig


class AcmevehicleConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'acmevehicle'
