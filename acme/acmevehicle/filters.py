from acmevehicle.models import Vehicle
from django.conf import settings
from django_filters import CharFilter, FilterSet
from rest_framework.pagination import PageNumberPagination


class CustomPageNumberPagination(PageNumberPagination):
    page_size_query_param = 'limit'
    page_size = settings.PAGINATION_SIZE


class VehicleFilter(FilterSet):
    class Meta:
        model = Vehicle
        fields = (
            'make',
            'type',
        )

    make = CharFilter(field_name='make', lookup_expr='icontains')
    type = CharFilter(field_name='type', lookup_expr='icontains')
    model = CharFilter(field_name='model', lookup_expr='icontains')
