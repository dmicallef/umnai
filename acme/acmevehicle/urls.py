from acmevehicle.viewsets import VehicleViewSet
from django.urls import include, path
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'vehicles', VehicleViewSet, basename='vehicles')

urlpatterns = [
    path('', include(router.urls)),
]
