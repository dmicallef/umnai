from acmevehicle.filters import CustomPageNumberPagination, VehicleFilter
from acmevehicle.models import Vehicle
from acmevehicle.serializers import VehicleSerializer
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.filters import OrderingFilter
from rest_framework.viewsets import ModelViewSet


class VehicleViewSet(ModelViewSet):
    queryset = Vehicle.objects.all()
    serializer_class = VehicleSerializer
    filter_class = VehicleFilter
    filter_backends = (DjangoFilterBackend, OrderingFilter)
    pagination_class = CustomPageNumberPagination
    ordering_fields = ('year',)
