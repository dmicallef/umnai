import logging
from django.core.validators import RegexValidator
from django.db import models
from django.utils.translation import gettext_lazy as _

logger = logging.getLogger(__name__)


class AbstractTrackedModel(models.Model):
    class Meta:
        abstract = True

    ctime = models.DateTimeField(auto_now_add=True, verbose_name=_('Creation time'))
    mtime = models.DateTimeField(auto_now=True, verbose_name=_('Update time'))

    def save(self, *args, **kwargs):
        update_fields = kwargs.get('update_fields')

        if isinstance(update_fields, list) and 'mtime' not in update_fields:
            update_fields.append('mtime')
        super().save(*args, **kwargs)


class Vehicle(AbstractTrackedModel):
    VEHICLE_TYPE_CHOICES = [
        ('car', 'Car'),
        ('truck', 'Truck'),
        ('motorcycle', 'Motorcycle'),
    ]

    class Meta:
        ordering = ["year"]

    alphanumeric = RegexValidator(r'^[0-9a-zA-Z]*$', 'Only alphanumeric characters are allowed.')

    vin = models.CharField(primary_key=True, unique=True, max_length=50, validators=[alphanumeric])
    type = models.CharField(max_length=15, choices=VEHICLE_TYPE_CHOICES)
    make = models.CharField(max_length=50)
    model = models.CharField(max_length=50, blank=True)
    year = models.PositiveSmallIntegerField()
    seat_capacity = models.PositiveSmallIntegerField()

    # Other fields
    roof_rack_availibility = models.BooleanField(
        null=True, default=None, verbose_name=_('Roof Rack Availability')
    )
    haul_capacity = models.PositiveIntegerField(null=True, verbose_name=_('Haul Capacity (kg)'))
    sidecar_availibility = models.BooleanField(null=True, verbose_name=_('Sidecar availibility'))
