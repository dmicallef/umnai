from acmevehicle.models import Vehicle
from rest_framework import serializers


class VehicleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Vehicle
        fields = (
            "vin",
            "type",
            "make",
            "model",
            "year",
            "seat_capacity",
            "roof_rack_availibility",
            "haul_capacity",
            "sidecar_availibility",
        )
